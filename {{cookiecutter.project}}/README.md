# Documentation of {{cookiecutter.project}}

## How to run the pytest
1. Install pytest
2. Run the pytest command in the terminal.
```
pytest
```
3. Check the coverage of the test with the following command.
```
pytest --cov
```